CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE SCHEMA taskbot;

CREATE TABLE taskbot.task_lists
(
    id UUID DEFAULT uuid_generate_v4(),
    chat_id BIGINT,
    list_date DATE,
    CONSTRAINT pk_task_lists PRIMARY KEY(id)
);

CREATE TABLE taskbot.tasks
(
    id UUID DEFAULT uuid_generate_v4(),
    list_id UUID,
    chat_id BIGINT,
    task_content TEXT,
    notification_time TIMESTAMPTZ,
    completed BOOL,
    CONSTRAINT pk_tasks PRIMARY KEY(id),
    CONSTRAINT fk_status_name FOREIGN KEY(list_id) REFERENCES taskbot.task_lists(id)
);

CREATE TABLE taskbot.daily_tasks
(
    id UUID DEFAULT uuid_generate_v4(),
    chat_id BIGINT,
    task_content TEXT,
    notification_time TIMESTAMPTZ,
    CONSTRAINT pk_daily_tasks PRIMARY KEY(id)
)